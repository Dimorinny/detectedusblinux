#-------------------------------------------------
#
# Project created by QtCreator 2014-11-17T02:13:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FindUsbModule
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    UsbFlash.cpp \
    UsbFinder.cpp

HEADERS  += mainwindow.h \
    UsbFlash.h \
    UsbFinder.h

FORMS    += mainwindow.ui
