#include "UsbFinder.h"
#include <QDebug>

UsbFinder::UsbFinder(QObject *parent) :
    QObject(parent) {

    blkidProcess = new QProcess(this);
    countsUsb    = 0;
    counter      = 0;
}

void UsbFinder::startFindUsb() {

    countsUsb    = 0;
    counter      = 0;

    blkidProcess->setProcessChannelMode(QProcess::MergedChannels);
    blkidProcess->start("blkid");

    connect(blkidProcess, SIGNAL(finished(int)), SLOT(blkidProcessFinishedSlot()));
}


void UsbFinder::blkidProcessFinishedSlot() {

    QRegExp regSda("((?!sda).)*$");
    QRegExp regNand("((?!nand).)*$");
    QString blkidResult = blkidProcess->readAll();
    QStringList blkidResultList = blkidResult.split("\n", QString::SkipEmptyParts);
    QStringList resultBlkidWorkList;

    for(int i = 0; i < blkidResultList.size(); i++) {

        if(regSda.indexIn(blkidResultList.at(i)) == 0
                && regNand.indexIn(blkidResultList.at(i)) == 0) {

            resultBlkidWorkList.append(blkidResultList.at(i).left(8));
        }
    }

    if(resultBlkidWorkList.isEmpty())
        emit finished(QList<UsbFlash>());
    else
        emit blkidFinishedStartUdiskSlot(resultBlkidWorkList);

}

void UsbFinder::blkidFinishedStartUdiskSlot(const QStringList& resultBlkidWorkList) {

    countsUsb = resultBlkidWorkList.size();

    for(int i = 0; i < countsUsb; i++) {

        QStringList args;
        args.append("--show-info");
        args.append(resultBlkidWorkList[i]);

        QProcess* currProcess = new QProcess(this);

        currProcess->setProcessChannelMode(QProcess::MergedChannels);
        currProcess->start("udisks", args);


        connect(currProcess, SIGNAL(finished(int)), SLOT(UdiskFinishedSlot()));
    }
}

void UsbFinder::UdiskFinishedSlot() {

    QProcess* senderProcess = dynamic_cast<QProcess*> (sender());

    if(senderProcess != NULL)
        usbList.append(UsbFlash(senderProcess->readAll()));

    if(++counter == countsUsb)
        emit finished(usbList);
}

