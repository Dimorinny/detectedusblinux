#ifndef USBFINDER_H
#define USBFINDER_H

#include <QObject>
#include <QStringList>
#include <QString>
#include <QRegExp>
#include <QProcess>
#include <QList>
#include "UsbFlash.h"

class UsbFinder : public QObject
{
    Q_OBJECT
public:
    explicit UsbFinder(QObject *parent = 0);
    void startFindUsb();
    
private:
    QProcess*        blkidProcess;
    QList<UsbFlash>  usbList;
    int              countsUsb;
    int              counter;



signals:
    void finished(const QList<UsbFlash>&);
    
private slots:
    void blkidProcessFinishedSlot();
    void blkidFinishedStartUdiskSlot(const QStringList&);
    void UdiskFinishedSlot();
    
};

#endif // USBFINDER_H
