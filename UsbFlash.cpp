#include "UsbFlash.h"
#include <QDebug>
#include <QRegExp>

UsbFlash::UsbFlash() :
    serial(""), name(""), vendor(""), path(""), size(0)
{}

UsbFlash::UsbFlash(const QString & _serial, const QString & _name, const QString & _vendor,
                   const QString & _path, u2long_t _size):
    serial(_serial), name(_name), vendor(_vendor), path(_path), size(_size)
{}

UsbFlash::UsbFlash(const QString &parseString):
    serial(""), name(""), vendor(""), path(""), size(0)
{
    QRegExp reg("  serial: *([^ \\n]+)");
    reg.indexIn(parseString);
    serial = reg.cap(1);

    reg.setPattern("  model: *([^ \\n]+)");
    reg.indexIn(parseString);
    name = reg.cap(1);

    reg.setPattern("  vendor: *([^ \\n]+)");
    reg.indexIn(parseString);
    vendor = reg.cap(1);

    reg.setPattern("  device-file: *([^ \\n]+)");
    reg.indexIn(parseString);
    path = reg.cap(1);

    reg.setPattern("  size: *([^ \\n]+)");
    reg.indexIn(parseString);
    size = reg.cap(1).toLongLong();
}


