#ifndef USBFLASH_H
#define USBFLASH_H

#include <QObject>
#include <QString>
#include <QProcess>
#include <QTimer>
#include <QDebug>

typedef unsigned long long u2long_t;


class UsbFlash
{

public:
    explicit UsbFlash();
    explicit UsbFlash(const QString&, const QString&, const QString&, const QString&, u2long_t);
    explicit UsbFlash(const QString& parseString);

    QString getSerial()  { return serial; }
    QString getName()    { return name;   }
    QString getVendor()  { return vendor; }
    QString getPath()    { return path;   }

    u2long_t getSize()   { return size;   }

    void debug() { qDebug() << serial << " " << name << " " << vendor << " " << path << " " << size; }

    
private:
    QString serial;
    QString name;
    QString vendor;
    QString path;

    u2long_t size;
};

#endif // USBFLASH_H
