#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QRegExp>
#include <QStringList>
#include "UsbFinder.h"
#include "UsbFlash.h"
#include <QTimer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QTimer* tm = new QTimer(this);


    //connect(tm, SIGNAL(timeout()), SLOT(tick()));
    //tm->start(3000);

    connect(ui->comboBox, SIGNAL(highlighted(int)), SLOT(activ(int)));
    connect(ui->pushButton, SIGNAL(clicked()), SLOT(onclick()));
    tick();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::finish(QList<UsbFlash> lol)
{
    l.clear();
    list.clear();

    for(int i = 0; i < lol.size(); i++) {
        l.append(lol[i].getName());
        list.append(lol[i]);
    }

    ui->comboBox->clear();
    ui->comboBox->addItems(l);
    emit activ(0);
}

void MainWindow::tick()
{

    qDebug() << " lpol";

    UsbFinder * finder = new UsbFinder(this);
    finder->startFindUsb();


    connect(finder, SIGNAL(finished(QList<UsbFlash>)), SLOT(finish(QList<UsbFlash>)));
}

void MainWindow::activ(int kol)
{
    ui->textEdit->clear();
    if(!list.isEmpty())
        ui->textEdit->append("Serial: " + list[kol].getSerial() + "\n Size: " + QString::number(list[kol].getSize())
                         + "\n Vendor: " + list[kol].getVendor() + "\n Name: " + list[kol].getName());
}

int MainWindow::onclick()
{
    tick();
}


