#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QDebug>
#include "UsbFlash.h"
#include <QList>
#include <QStringList>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;
    QProcess* lol;
    QList<UsbFlash> list;
    QStringList l;

private slots:
    void finish(QList<UsbFlash>);
    void tick();
    void activ(int);
    int onclick();

};

#endif // MAINWINDOW_H
